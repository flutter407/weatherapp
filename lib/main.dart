import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';

import 'aspect_ratio.dart';

void main() {
  runApp(const ExampleApp());
}

class ExampleApp extends StatefulWidget {
  const ExampleApp({Key? key}) : super(key: key);

  @override
  State<ExampleApp> createState() => _ExampleAppState();
}

class _ExampleAppState extends State<ExampleApp> {
  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'Responsive and adaptive UI in Flutter',
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
        home: HomePage(),
      ),
    );
  }
}


class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          image: DecorationImage(
        fit: BoxFit.fill,
        image: 
        AssetImage('assets/images/Sky.jpg'),
        // NetworkImage(
        //   'https://i.pinimg.com/originals/d7/6e/5a/d76e5af83f4996f5933aaf105d1e9b59.jpg',
        // ),
      )),
      child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            title: Text(
              "เมืองชลบุรี",
              style: TextStyle(fontSize: 25),
            ),
            backgroundColor: Colors.transparent,
            elevation: 0.0,
            actions: <Widget>[
              IconButton(
                  onPressed: () {},
                  icon: Icon(
                    Icons.settings,
                    // color: Colors.black,
                  )),
              IconButton(
                  onPressed: () {},
                  icon: Icon(
                    Icons.more_vert,
                    // color: Colors.black,
                  )),
            ],
          ),
          body: ListView(
            children: [
              Container(
                color: Colors.transparent,
                child: Column(children: [
                  Container(
                    // color: Colors.pink,
                    width: 300,
                    height: 180,
                    // alignment: Alignment.center,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              const Text(
                                '25',
                                style: TextStyle(
                                    fontSize: 80, color: Colors.white),
                              ),
                            ]),
                        Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              const Text(
                                '\u2103',
                                style: TextStyle(
                                    fontSize: 30, color: Colors.white),
                              ),
                            ]),
                      ],
                    ),
                  ),
                  Container(
                      color: Colors.transparent,
                      child: Column(children: [
                        const Text(
                          'มีเมฆเป็นส่วนใหญ่ 25~30\u2103',
                          style: TextStyle(fontSize: 20, color: Colors.white),
                        )
                      ]))
                ]),
              ),
              Container(
                height: 60,
              ),
              Container(
                child: Center(
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,

                        children: [
                          for (int i = 0; i < 12; i++) buildSunnyWithHour(i),
                        ]),
                  ),
                ),
              ),
              Container(
                height: 20,
              ),
              Container(
                // color: Colors.amber,
                
                child: Column(
                  children: [
                    Container(
                      width: 380,
                      height: 250,
                      // color: Colors.red,
                      decoration: BoxDecoration(
                    border: Border.all(width: 15, color: Colors.transparent),
                    borderRadius: BorderRadius.circular(50),
                    boxShadow: [
                      BoxShadow(color:Colors.white.withOpacity(0.3),),
                    ]
                    ),
                      child: SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              for (int i = 0; i < 12; i++) buildListWeather(),
                            ]),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: 500,
              )
              // color: ,
            ],
          )),
    );
  }
}

Widget buildSunnyWithHour(int x) {
  return Padding(
    // if(x%2 != 0){}
    padding: const EdgeInsets.all(8.0),
    child: Column(
      children: <Widget>[
        Icon(
          Icons.sunny,
          color: Colors.amber,
          size: 30,
        ),
        Text("$x:00"),
      ],
    ),
  );
}

Widget buildListWeather() {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text(
          "28/12",
          style: TextStyle(fontSize: 18, color: Colors.white),
        ),
        Text(
          "วันนี้",
          style: TextStyle(fontSize: 18, color: Colors.white),
        ),
        Icon(
          Icons.cloud_circle_rounded,
          color: Color.fromARGB(255, 7, 115, 216),
          size: 30,
        ),
        Text(
          "ต่ำสุด",
          style: TextStyle(fontSize: 18, color: Colors.white),
        ),
        Text(
          "สูงสุด",
          style: TextStyle(fontSize: 18, color: Colors.white),
        ),
      ],
    ),
  );
}
